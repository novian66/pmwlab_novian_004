import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-person-input',
  templateUrl:'./person-input.component.html',
  styleUrls: ['./person-input.component.css']
})
export class PersonInputComponent{
  @Output() personCreate = new EventEmitter<String>();
  enteredPersonName = "";
  // onCreatePerson(personName: string){
  //   console.log('Data Mahasiswa :' + personName);
  // }
  onCreatePerson(){
    this.personCreate.emit(this.enteredPersonName);
    console.log('Data Mahasiswa :' + this.enteredPersonName);
  }
}
