import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiscoverPageModule } from './discover/discover.module';
import { PlaceDetailPageModule } from './discover/place-detail/place-detail.module';
import { EditOfferPageModule } from './offers/edit-offer/edit-offer.module';
import { NewOfferPageModule } from './offers/new-offer/new-offer.module';
import { OfferBookingsPageModule } from './offers/offer-bookings/offer-bookings.module';
import { OffersPageModule } from './offers/offers.module';

import { PlacesPage } from './places.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: PlacesPage,
    children:[
      {
        path: 'discover',
        loadChildren: () => import('./discover/discover.module').then(m=>DiscoverPageModule),
        children:[
          {
            path: '',
            loadChildren: () => import('./discover/discover.module').then(m=>DiscoverPageModule)
          },
          {
            path: ':placeId',
            loadChildren: () => import('./discover/place-detail/place-detail.module').then(m=>PlaceDetailPageModule)
          }
        ]
      },
      {
        path: 'offers',
        loadChildren: () => import('./offers/offers.module').then(m=>OffersPageModule),
        children:[
          {
            path: '',
            loadChildren: () => import('./offers/offers.module').then(m=>OffersPageModule)
          },
          {
            path: 'new',
            loadChildren: () => import('./offers/new-offer/new-offer.module').then(m=>NewOfferPageModule)
          },
          {
            path: 'edit/:placeId',
            loadChildren: () => import('./offers/edit-offer/edit-offer.module').then(m=>EditOfferPageModule)
          },
          {
            path: ':placeId',
            loadChildren: () => import('./offers/offer-bookings/offer-bookings.module').then(m=>OfferBookingsPageModule)
          }
        ]
      }
    ]
  },
  {
    path:'',
    redirectTo: '/places/tabs/discover',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlacesPageRoutingModule {}
