import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { take, map, tap, delay } from 'rxjs/operators';

import { Place } from './place.model';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _places = new BehaviorSubject<Place[]>([
    new Place(
      'p1' ,
      'Baturraden' ,
      'Objek wisata keluarga di Purwokerto yang berikutnya adalah Kebun Raya Baturraden. Lokasi wisata keluarga ini berada di Jl. Pancurantujuh – Wanawisata Baturraden, Purwokerto, Dusun III Berubahan, Kemutug Lor, Kec. Baturaden, Kabupaten Banyumas, Jawa Tengah.',
      'https://mytrip123.com/wp-content/uploads/2015/12/Baturaden.jpg',
      75000,
      new Date('2020-12-25'),
      new Date('2020-12-30'),
      '12'
    ),
    new Place(
      'p2' ,
      'Curug Gede' ,
      'Curug Gede menawarkan pemandangan alam dengan pepohonan rimbun dan aliran debit air setinggi 50 meter yang cukup deras.',
      'https://i2.wp.com/ranggawisata.com/wp-content/uploads/2019/11/Curug-Gede.jpg',
       50000,
       new Date('2020-12-25'),
       new Date('2020-12-30'),
       '12'
    ),
    new Place(
      'p3' ,
      'Bukit Watu Meja' ,
      'Objek wisata alam di Purwokerto yang berikutnya adalah Bukit Watu Meja. Lokasi wisata alam ini berada di Jl. Raya Patikraja – Kebasen, Tumiyang Kidul, Tumiyang, Kec. Kebasen, Kabupaten Banyumas, Jawa Tengah.',
      'https://i1.wp.com/ranggawisata.com/wp-content/uploads/2019/11/Bukit-Watu-Meja.jpg',
       90000,
       new Date('2020-12-25'),
       new Date('2020-12-30'),
       '12'
       )
      ]);

  get places() {
    return this._places.asObservable();
  }

  constructor(private authService: AuthService) {}

  getPlace(id: string) {
    return this.places.pipe(
      take(1),
      map(places => {
        return { ...places.find(p => p.id === id) };
      })
    );
  }

  addPlace(
    title: string,
    description: string,
    price: number,
    dateFrom: Date,
    dateTo: Date
  ) {
    const newPlace = new Place(
      Math.random().toString(),
      title,
      description,
      'https://lonelyplanetimages.imgix.net/mastheads/GettyImages-538096543_medium.jpg?sharp=10&vib=20&w=1200',
      price,
      dateFrom,
      dateTo,
      this.authService.userId
    );
    return this.places.pipe(
      take(1),
      delay(1000),
      tap(places => {
        this._places.next(places.concat(newPlace));
      })
    );
  }
  updatePlace(placeId: string, title: string, description: string) {
    return this.places.pipe(
      take(1),
      delay(1000),
      tap(places => {
        const updatedPlaceIndex = places.findIndex(pl => pl.id === placeId);
        const updatedPlaces = [...places];
        const oldPlace = updatedPlaces[updatedPlaceIndex];
        updatedPlaces[updatedPlaceIndex] = new Place(
          oldPlace.id,
          title,
          description,
          oldPlace.imageUrl,
          oldPlace.price,
          oldPlace.availableFrom,
          oldPlace.availableTo,
          oldPlace.userId
        );
        this._places.next(updatedPlaces);
      })
    );
  }
}
