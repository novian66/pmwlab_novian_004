import { Injectable } from '@angular/core';
import { Place } from './place.model';
@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _places: Place[] = [
    new Place(
      'p1' ,
      'Baturraden' ,
      'Objek wisata keluarga di Purwokerto yang berikutnya adalah Kebun Raya Baturraden. Lokasi wisata keluarga ini berada di Jl. Pancurantujuh – Wanawisata Baturraden, Purwokerto, Dusun III Berubahan, Kemutug Lor, Kec. Baturaden, Kabupaten Banyumas, Jawa Tengah.',
      'https://mytrip123.com/wp-content/uploads/2015/12/Baturaden.jpg',
      75000,
      new Date('2020-12-25'),
      new Date('2020-12-30'),
    ),
    new Place(
      'p2' ,
      'Curug Gede' ,
      'Curug Gede menawarkan pemandangan alam dengan pepohonan rimbun dan aliran debit air setinggi 50 meter yang cukup deras.',
      'https://i2.wp.com/ranggawisata.com/wp-content/uploads/2019/11/Curug-Gede.jpg',
       50000,
       new Date('2020-12-25'),
       new Date('2020-12-30'),
    ),
    new Place(
      'p3' ,
      'Bukit Watu Meja' ,
      'Objek wisata alam di Purwokerto yang berikutnya adalah Bukit Watu Meja. Lokasi wisata alam ini berada di Jl. Raya Patikraja – Kebasen, Tumiyang Kidul, Tumiyang, Kec. Kebasen, Kabupaten Banyumas, Jawa Tengah.',
      'https://i1.wp.com/ranggawisata.com/wp-content/uploads/2019/11/Bukit-Watu-Meja.jpg',
       90000,
       new Date('2020-12-25'),
       new Date('2020-12-30'),
    ),
  ];

  get places(){
    return [...this._places];
  }

  constructor() { }
  getPlace(id: string) {
    return {...this._places.find(p => p.id === id)};
  }
}
